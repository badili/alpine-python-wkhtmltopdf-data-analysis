FROM ghcr.io/surnet/alpine-python-wkhtmltopdf:3.12.2-0.12.6-full

MAINTAINER Wangoru Kihara wangoru.kihara@badili.co.ke

## install npm from alpine-3.10 repo
# RUN echo 'https://dl-cdn.alpinelinux.org/alpine/latest-stable/main' > /tmp/apk.repo && apk add nodejs nodejs-npm --repositories-file=/tmp/apk.repo && rm -f /tmp/apk.repo

# resort to the repositories defined in the alpine-python-wkhtmltopdf
RUN apk add --update --no-cache \
    mariadb-client \
    mariadb-connector-c-dev \
    libc-dev \
    libxml2 \
    build-base \
    git curl wget bash \
    g++ gcc libxslt-dev \
    postgresql-libs postgresql-dev \
    zlib-dev freetype-dev  \
    lcms2-dev openjpeg-dev \
    tk-dev harfbuzz-dev fribidi-dev \
    tar \
    libffi-dev \
    jpeg-dev zlib-dev \
    bash coreutils \
    openjdk8-jre \
    geos gdal \
    geos-dev gdal-dev \
    tzdata \
    nodejs npm

# RUN pip install GDAL
RUN pip install --upgrade pip
# RUN pip install Cython --install-option="--no-cython-compile"
RUN pip install pandas==1.5.0
RUN pip install matplotlib==3.8.4
RUN pip install cryptography==3.3.2
RUN pip install plotly==5.13.0
RUN pip install numpy==1.26.4
